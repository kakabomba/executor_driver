# Simple Linux Executor Driver


Allows to create files with executable content which run on read event

Often you have to use something that rely on local file, but you need generate it dynamicaly

This driver acts as follows:

```bash
echo 'echo time is $(date)' > /dev/executor
cat /dev/executor #time is Fri May  4 21:21:04 EEST 2018
sleep 2
cat /dev/executor #time is Fri May  4 21:21:06 EEST 2018
```

See install.sh for details
